
export class IEmployee
{
    Id : number;
    FirstName :  string;
    LastName :  string;
    JoinedDateTime : Date;
    IsPermenent : boolean;
    IsFullTime : boolean;
    Salary : number;
    IsActive : boolean;
    ManagerId : number;
    DepartmentId : number;
    PositionId : number;

    ManagerName : string;
    DepartmentName : string;
    JobTitle : string;
    IsPermenentString : string;
    IsFullTimeString : string;
    JoinedDateString : string;
}